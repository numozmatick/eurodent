const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer')
const cleanCSS = require('gulp-clean-css')

gulp.task('sass-compile', function () {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('all.css'))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/build/css'))
})

gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss', gulp.series('sass-compile'))
})

